gnome-desktop-testing (2021.1-4+apertis0) apertis; urgency=medium

  * Sync from debian/trixie.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Mar 2025 18:08:53 +0000

gnome-desktop-testing (2021.1-4) unstable; urgency=medium

  [ Amin Bandali ]
  * Switch packaging branch to debian/latest

  [ Simon McVittie ]
  * d/upstream/metadata: Add Repository
  * d/control: Stop generating from a template
  * Standards-Version: 4.7.0 (no changes required)

 -- Simon McVittie <smcv@debian.org>  Tue, 05 Nov 2024 13:12:23 +0000

gnome-desktop-testing (2021.1-3+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Tue, 13 Jun 2023 17:24:17 +0000

gnome-desktop-testing (2021.1-3) unstable; urgency=medium

  [ Jeremy Bicha ]
  * Update d/watch

  [ Simon McVittie ]
  * Remove version constraints unnecessary since buster (oldstable)
  * d/changelog: Trim trailing whitespace
  * d/upstream/metadata: Add
  * Update standards version to 4.6.2, no changes needed.
  * Avoid explicitly specifying -Wl,--as-needed linker flag.
    This is unnecessary with current toolchains.

 -- Simon McVittie <smcv@debian.org>  Sun, 22 Jan 2023 12:55:37 +0000

gnome-desktop-testing (2021.1-2) unstable; urgency=medium

  * Release to unstable

 -- Simon McVittie <smcv@debian.org>  Mon, 16 Aug 2021 10:06:48 +0100

gnome-desktop-testing (2021.1-1) experimental; urgency=medium

  * New upstream release
  * Restore old d/watch.
    The new upstream release has a tarball again.
  * Standards-Version: 4.5.1 (no changes required)
  * Bump debhelper compat level to 13

 -- Simon McVittie <smcv@debian.org>  Thu, 01 Jul 2021 14:18:23 +0100

gnome-desktop-testing (2018.1+git20210629-1) experimental; urgency=medium

  * New upstream snapshot
    - Automatically create --log-directory
    - Make --help less confusing
    - Add a man page
    - Avoid using n_passed, n_skipped, n_failed uninitialized
  * Drop patches that were applied upstream
  * Add myself to Uploaders

 -- Simon McVittie <smcv@debian.org>  Tue, 29 Jun 2021 16:19:16 +0100

gnome-desktop-testing (2018.1-3apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 23:20:20 +0000

gnome-desktop-testing (2018.1-3) unstable; urgency=medium

  * Team upload
  * Mark gnome-desktop-testing as Multi-Arch: foreign.
    It only provides an executable interface, so when carrying out
    cross-testing it is OK to use the version from the testbed's
    architecture. Thanks to Steve Langasek. (Closes: #946298)
  * d/p/Don-t-crash-on-unknown-command-line-options.patch:
    Add patch from upstream git to fix a crash on unknown CLI options
  * Standards-Version: 4.4.1 (no changes required)

 -- Simon McVittie <smcv@debian.org>  Mon, 09 Dec 2019 16:06:12 +0000

gnome-desktop-testing (2018.1-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 15 Feb 2021 11:43:28 +0000

gnome-desktop-testing (2018.1-2) unstable; urgency=medium

  [ Jeremy Bicha ]
  * Restore -Wl,-O1 to our LDFLAGS
  * Bump Standards-Version to 4.3.0

  [ Simon McVittie ]
  * d/watch: Adapt for upstream migration to GNOME Gitlab

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 23 Dec 2018 10:57:41 -0500

gnome-desktop-testing (2018.1-1) unstable; urgency=medium

  * Team upload

  [ Jeremy Bicha ]
  * Update Vcs fields for migration to https://salsa.debian.org/

  [ Simon McVittie ]
  * New upstream release
  * d/p/Factor-out-test_log-to-wrap-sd_journal_send.patch,
    d/p/Output-messages-by-default-if-stdout-is-not-the-Journal.patch,
    d/p/Make-libsystemd-optional-again.patch,
    d/p/Add-a-TAP-output-mode.patch:
    Add patches from upstream git to re-introduce portability to
    non-Linux systems (and add a --tap option)
  * Bump Standards-Version to 4.1.4
  * Remove dependency on libgsystem, no longer required
  * Use debhelper 11
  * Drop obsolete build-dependency on libgirepository1.0-dev

 -- Simon McVittie <smcv@debian.org>  Tue, 15 May 2018 12:50:13 +0100

gnome-desktop-testing (2016.1-2) unstable; urgency=medium

  * Update Vcs fields for conversion to git
  * Add debian/gbp.conf
  * Bump debhelper compat to 10
  * Bump Standards-Version to 4.1.2

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 14 Dec 2017 21:37:58 -0500

gnome-desktop-testing (2016.1-1) unstable; urgency=medium

  * Team upload

  [ Jackson Doak ]
  * New upstream release
  * Drop 0001-runner-Exit-with-code-2-if-any-test-failed.patch, fixed upstream

  [ Simon McVittie ]
  * New upstream release
    - use the automatic git export from git.gnome.org, tarballs have not been
      uploaded for 2015.1 or 2016.1
    - debian/watch: watch for new git tags, not new tarballs
    - use dh-autoreconf to fill in missing build files
    - use system copy of libgsystem, the submodule was removed in 2015.1
    - debian/copyright: drop libgsystem stanza
  * Replace cdbs with dh (updating compat level to 9)
  * Standards-version: 3.9.8 (no changes required)
  * Update Homepage to
    https://wiki.gnome.org/Initiatives/GnomeGoals/InstalledTests

 -- Simon McVittie <smcv@debian.org>  Tue, 23 Aug 2016 09:00:53 +0100

gnome-desktop-testing (2013.1-2) unstable; urgency=low

  [ Jeremy Bicha ]
  * Update homepage

  [ Iain Lane ]
  * debian/patches/0001-runner-Exit-with-code-2-if-any-test-failed.patch:
    Backport from upstream to actually exit with a bad status if any tests
    fail, rather than 0.

 -- Iain Lane <laney@debian.org>  Mon, 28 Oct 2013 23:32:51 +0000

gnome-desktop-testing (2013.1-1) unstable; urgency=low

  * Initial release (Closes: #713959)

 -- Iain Lane <laney@debian.org>  Wed, 26 Jun 2013 10:19:11 +0100
